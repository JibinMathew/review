from nltk.stem.porter import PorterStemmer
stemmer = PorterStemmer()

def review_stem(x):
    try:
        return stemmer.stem(x)
    except:
        return x