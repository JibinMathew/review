def get_related_review(data,review_term):
    req_review = []

    if "Reviews" in data:
        for review in data['Reviews']:
            if review_term in review['Content']:
                req_review.append(review['Content'].encode('UTF-8'))
        return req_review
    else:
        return None