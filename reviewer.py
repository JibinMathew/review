from readjson import read_json
from readreview import get_related_review
from getSentences import split_into_sentences
from stemmer import review_stem
import string

def remove_punctuation(s):
    return s.translate(None, string.punctuation)

def calc_dist(sent_obj,required_word):
    print "\n"
    print sent_obj['sentence']
    print sent_obj['sentiment']
    print sent_obj['sentiment_word']
    sent_obj['sentence'] = remove_punctuation(sent_obj['sentence'])
    index_of_requiredword = map(review_stem, sent_obj["sentence"].split()).index(required_word)

    if len(sent_obj["sentiment_word"]) > 1:
        min_distance = len(sent_obj["sentence"])
        for word in sent_obj["sentiment_word"]:
            index_of_sentimentword = map(review_stem, sent_obj["sentence"].split()).index(word)
            if abs(index_of_requiredword - index_of_sentimentword) < min_distance:
                min_distance = abs(index_of_requiredword - index_of_sentimentword)
                real_senti = word
                real_senti_index = index_of_sentimentword
    else:
        real_senti = sent_obj['sentiment_word'][0]
        real_senti_index = map(review_stem, sent_obj["sentence"].split()).index(real_senti)

    real_int = []
    if len(sent_obj["intensifier"]) > 1 and len(sent_obj["sentiment_word"]) > 1:
        for word in sent_obj["intensifier"]:
            index_of_intensifierword = map(review_stem, sent_obj["sentence"].split()).index(word)
            min_distance = abs(real_senti_index - index_of_intensifierword)
            found = 0
            for sent in sent_obj["sentiment_word"]:
                index_of_sentimentword = map(review_stem, sent_obj["sentence"].split()).index(sent)
                if abs(index_of_sentimentword - index_of_intensifierword) < min_distance:
                    found = 1
                    continue
            if found == 0:
                real_int.append(word)

    elif (len(["intensifier"]) > 1 or len(sent_obj["intensifier"]) == 1) and len(sent_obj["sentiment_word"]) == 1:
        ind_of_brk = map(review_stem, sent_obj["sentence"].split()).index(sent_obj["sentiment_word"][0])
        min_distance = len(sent_obj["sentence"])
        for word in sent_obj["intensifier"]:
            ind_word = map(review_stem, sent_obj["sentence"].split()).index(word)
            if abs(ind_of_brk - ind_word) < min_distance:
                min_distance = abs(ind_of_brk - ind_word)
                temp = word
        try:
            real_int.append(temp)
        except:
            pass
    print "real intensifier : "
    print real_int
    print  "real sentiment : "
    print real_senti

def reviewer(review_term):

    readfile = read_json("reviews2")
    if readfile == None:
        return None
    required_review = get_related_review(readfile,review_term)

    if required_review == None:
        return None

    count_reviews = len(required_review)
    required_sentence = map(lambda x,y : split_into_sentences(x,y),required_review,[review_term]*count_reviews)

    import itertools
    required_sentence = list(itertools.chain(*required_sentence))


    import json
    sentiment = json.load(open('semantics.json', 'r'))

    positive_value = dict()
    negative_value = dict()
    intensifiers_value = dict()


    pWords = []
    nWords = []
    iWords = []

    for item in sentiment['positive']:
        word = review_stem(item['phrase'].encode('utf-8'))
        positive_value[word] = item['value']
        pWords.append(word)

    for item in sentiment['negative']:
        word = review_stem(item['phrase'].encode('utf-8'))
        negative_value[word] = item['value']
        nWords.append(word)

    for item in sentiment['intensifier']:
        word = review_stem(item['phrase'].encode('utf-8'))
        intensifiers_value[word] = item['multiplier']
        iWords.append(word)

    sentiment_matrix = []

    def sentiment_tagger(sentence):
        sentiment_object = dict()
        sentiment_object['sentiment_word'] = []
        sentence_array = map(review_stem,sentence.split())
        sentiment_found = False

        for word in pWords: # postive word can be a combination on more than one word
            count = len(word.split())
            if count > 1:
                if word in sentence:
                    sentiment_object['sentence'] = sentence
                    sentiment_object['sentiment_word'].append(word)
                    sentiment_object["sentiment"] = 1
                    sentiment_found = True

            else:
                if word in sentence_array:
                    sentiment_object['sentence'] = sentence
                    sentiment_object['sentiment_word'].append(word)
                    sentiment_object["sentiment"] = 1
                    sentiment_found = True

        for word in nWords: # negative word can be a combination on more than one word
            count = len(word.split())
            if count > 1:
                if word in sentence:
                    sentiment_object['sentence'] = sentence
                    sentiment_object['sentiment_word'].append(word)
                    sentiment_object["sentiment"] = 0
                    sentiment_found = True

            else:
                if word in sentence_array:
                    sentiment_object['sentence'] = sentence
                    sentiment_object['sentiment_word'].append(word)
                    sentiment_object["sentiment"] = 0
                    sentiment_found = True

        if sentiment_found:
            sentiment_object["intensifier"] = []
            for word in iWords:
                if word in sentence_array:
                    sentiment_object['intensifier'].append(word)

            sentiment_matrix.append(sentiment_object)

    map(sentiment_tagger,required_sentence)
    relevant_review = len(sentiment_matrix)
    map(calc_dist,sentiment_matrix,[review_term]*relevant_review)


reviewer("breakfast")