def read_json(filename):
    import json
    try:
        data = json.load(open(filename+'.json', 'r'))
        return data
    except:
        return None